DEBOS_CMD=docker
$DEBOS_CMD cp mobianinstaller:/tmp/mobian.img mobian.img
$DEBOS_CMD cp mobianinstaller:/tmp/linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.diff.gz linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.diff.gz
$DEBOS_CMD cp mobianinstaller:/tmp/linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.dsc linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.dsc
$DEBOS_CMD cp mobianinstaller:/tmp/linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.orig.tar.gz linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.orig.tar.gz
$DEBOS_CMD cp mobianinstaller:/tmp/linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.buildinfo linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.buildinfo
$DEBOS_CMD cp mobianinstaller:/tmp/linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.changes linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.changes
$DEBOS_CMD cp mobianinstaller:/tmp/linux-headers-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.deb linux-headers-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.deb
$DEBOS_CMD cp mobianinstaller:/tmp/linux-image-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.deb linux-image-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.deb
$DEBOS_CMD cp mobianinstaller:/tmp/linux-libc-dev_5.8.0+megi+pinephone+mobian_arm64.deb linux-libc-dev_5.8.0+megi+pinephone+mobian_arm64.deb
$DEBOS_CMD cp mobianinstaller:/tmp/initrd.img-new initrd5.8.0+megi+pinephone+mobian.img
$DEBOS_CMD cp mobianinstaller:/tmp/crust-meta/build/pinephone/u-boot-sunxi-with-spl.bin u-boot-sunxi-with-spl.bin
$DEBOS_CMD exec mobianinstaller umount /media/boot/
$DEBOS_CMD exec mobianinstaller umount /media/root/
$DEBOS_CMD exec mobianinstaller /tmp/unlinknode.sh
$DEBOS_CMD exec mobianinstaller losetup -D
$DEBOS_CMD exec mobianinstaller unlink /tmp/recovery-pinephone-loop0
$DEBOS_CMD exec mobianinstaller rm /tmp/mobian.img
$DEBOS_CMD stop mobianinstaller
