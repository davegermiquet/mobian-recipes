#!/bin/sh

export PATH=/sbin:/usr/sbin:$PATH
IMG_FILE="mobian-$device-`date +%Y%m%d`.img"
DEBOS_CMD=docker 
ARGS="run --privileged=true --name mobianinstaller -dit amd64/debian /bin/bash"


# pull the buster image


$DEBOS_CMD pull amd64/debian

# create new docker of mobian installer or start it if existing


$DEBOS_CMD $ARGS || docker start mobianinstaller

# copy over needed files
$DEBOS_CMD cp build.sh mobianinstaller:/tmp
$DEBOS_CMD cp createBootImage.sh mobianinstaller:/tmp
$DEBOS_CMD cp initrd.img.template mobianinstaller:/tmp
$DEBOS_CMD cp unlinknode.sh mobianinstaller:/tmp
$DEBOS_CMD cp createImageSecond.sh mobianinstaller:/tmp
$DEBOS_CMD cp createnode.sh mobianinstaller:/tmp
$DEBOS_CMD cp backup.sh mobianinstaller:/tmp
$DEBOS_CMD cp boot.cmd mobianinstaller:/tmp
$DEBOS_CMD cp ansible-image.yml mobianinstaller:/tmp
$DEBOS_CMD cp custom_config mobianinstaller:/tmp


# start running docker commands
# install necessary dependencies for compilation

$DEBOS_CMD exec mobianinstaller /bin/bash -c "cd /tmp;/bin/ls"
$DEBOS_CMD exec mobianinstaller df -h
$DEBOS_CMD exec mobianinstaller /usr/bin/apt-get -y update
$DEBOS_CMD exec mobianinstaller  dpkg --add-architecture arm64
$DEBOS_CMD exec mobianinstaller /usr/bin/apt-get -y install ansible f2fs-tools debootstrap git \
crossbuild-essential-arm64 parted flex bison python3-distutils \
 swig python3-dev u-boot-tools build-essential device-tree-compiler \
bison flex libssl-dev libncurses-dev bc initramfs-tools qemu-utils qemu-efi-aarch64\
 qemu-system-arm  binfmt-support qemu qemu-user-static  python3-pip \
 cpio rsync dpkg-dev fakeroot e2fsprogs mount

# run playbook
$DEBOS_CMD exec mobianinstaller /usr/bin/ansible-playbook /tmp/ansible-image.yml

# copy over artifacts for downloading

$DEBOS_CMD cp mobianinstaller:/tmp/mobian.img mobian.img
$DEBOS_CMD cp mobianinstaller:/tmp/linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.diff.gz linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.diff.gz
$DEBOS_CMD cp mobianinstaller:/tmp/linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.dsc linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.dsc
$DEBOS_CMD cp mobianinstaller:/tmp/linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.orig.tar.gz linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian.orig.tar.gz
$DEBOS_CMD cp mobianinstaller:/tmp/linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.buildinfo linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.buildinfo
$DEBOS_CMD cp mobianinstaller:/tmp/linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.changes linux-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.changes
$DEBOS_CMD cp mobianinstaller:/tmp/linux-headers-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.deb linux-headers-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.deb
$DEBOS_CMD cp mobianinstaller:/tmp/linux-image-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.deb linux-image-5.8.0-rc4_5.8.0+megi+pinephone+mobian_arm64.deb
$DEBOS_CMD cp mobianinstaller:/tmp/linux-libc-dev_5.8.0+megi+pinephone+mobian_arm64.deb linux-libc-dev_5.8.0+megi+pinephone+mobian_arm64.deb
$DEBOS_CMD cp mobianinstaller:/tmp/initrd.img-new initrd5.8.0+megi+pinephone+mobian.img
$DEBOS_CMD cp mobianinstaller:/tmp/crust-meta/build/pinephone/u-boot-sunxi-with-spl.bin u-boot-sunxi-with-spl.bin


# cleanup all the old unnecessary for next build

$DEBOS_CMD exec mobianinstaller umount /media/boot/
$DEBOS_CMD exec mobianinstaller umount /media/root/
$DEBOS_CMD exec mobianinstaller /tmp/unlinknode.sh
$DEBOS_CMD exec mobianinstaller losetup -D
$DEBOS_CMD exec mobianinstaller unlink /tmp/recovery-pinephone-loop0
$DEBOS_CMD exec mobianinstaller rm /tmp/mobian.img
$DEBOS_CMD stop mobianinstaller