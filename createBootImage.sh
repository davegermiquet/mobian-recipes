mkdir /tmp/initrdnew
cp /tmp/initrd.img.template initrdnew
cd /tmp/initrdnew
zcat initrd.img.template | cpio -idmv
mkdir -p /tmp/initrdnew/etc/initramfs-tools/
cat > /tmp/initrdnew/etc/initramfs-tools/modules << EOF
f2fs
fscrypto
crc32-pclmul
crc32c_generic
crc32c-intel
crc32_generic
libcrc32c
EOF

rm -rf lib/modules/

rsync -avh /media/root/lib/modules/ lib/modules
find . | cpio -o -c | gzip -9 > /tmp/initrd.img-new
pwd
cp /tmp/initrd.img-new /media/boot/boot/initrd.img
mkimage -A arm64 -O linux -T ramdisk -C gzip -n "Build Root File System" -d /media/boot/boot/initrd.img /media/boot/boot/initrd.img.uboot
mv /media/boot/boot/initrd.img.uboot /media/boot/boot/initrd.img
mkimage -C none -A arm64 -T script -d /tmp/boot.cmd /media/boot/boot/boot.scr
ls -la /media/boot/boot
cd /

