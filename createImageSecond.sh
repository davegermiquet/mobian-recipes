# Setting up serial
echo "T0:23:respawn:/sbin/getty -L ttyS0 115200 vt100" > /arm64fs_debian/etc/inittab
chroot /arm64fs_debian sh -c "cd /dev; mknod -m 666 ttyS0 c 4 64"
cp /etc/hosts /arm64fs_debian/etc/

# Our proxy is apt-cacher-ng, which can't handle SSL connections
unset http_proxy

# Allow non-free components
echo "deb http://deb.debian.org/debian sid main contrib non-free" > /arm64fs_debian/etc/apt/sources.list

# allow mobian sources

cat > /arm64fs_debian/etc/apt/sources.list.d/extrepo_mobian.sources << EOF
Components: main non-free
Types: deb deb-src
Suites: mobian
Uris: http://repo.mobian-project.org/
Architectures: amd64 arm64 armhf
Signed-By: /var/lib/extrepo/keys/mobian.asc
EOF

# Add KEY
wget -O - https://repo.mobian-project.org/mobian.gpg.key > /arm64fs_debian/tmp/mobian.gpg.key

chroot /arm64fs_debian cat /tmp/mobian.gpg.key |  apt-key add -
mkdir -p /arm64fs_debian/var/lib/extrepo/keys/
cp /arm64fs_debian/tmp/mobian.gpg.key /arm64fs_debian/var/lib/extrepo/keys/mobian.asc

cat > /arm64fs_debian/etc/apt/preferences.d/00-mobian-priority << EOF
Package: *
Pin: release o=Mobian
Pin-Priority: 700

Package: *
Pin: release o=Debian
Pin-Priority: 500
EOF

# Install optional packages

chroot /arm64fs_debian /usr/bin/apt-get update
chroot /arm64fs_debian /usr/bin/apt-get install  -y --no-install-recommends mobian-phosh \
       mobian-phosh-games \
       eog \
       epiphany-browser \
       firefox-esr \
       fractal \
       gnome-authenticator \
       gnome-sound-recorder \
       gnome-todo \
       powersupply \
       telegram-desktop \
       webext-ublock-origin  \
        mobian-phosh-phone \
        linux-image-5.7-pinephone- \
        u-boot-sunxi-



BOOTUID=$(blkid -s UUID -o value /tmp/recovery-pinephone-loop0p1 | tr '\n' ' ')
ROOTUID=$(blkid -s UUID -o value /tmp/recovery-pinephone-loop0p2 | tr '\n' ' ')

echo "UUID=${ROOTUID}     /       f2fs   defaults       0       0" > /arm64fs_debian/etc/fstab
echo "UUID=${BOOTUID}     /boot   ext2   defaults      0       0" >> /arm64fs_debian/etc/fstab

# Setup hostname
echo mobian > /arm64fs_debian/etc/hostname

# Generate locales (only en_US.UTF-8 for now)
sed -i -e '/en_US\.UTF-8/s/^# //g' /arm64fs_debian/etc/locale.gen

# Load phosh on startup if package is installed
chroot /arm64fs_debian  systemctl enable phosh.service

export USERNAME=mobian
export PASSWORD=1234

chroot /arm64fs_debian adduser --gecos $USERNAME --disabled-password --shell /bin/bash $USERNAME
chroot /arm64fs_debian adduser $USERNAME sudo

# Needed for hardware access rights
chroot /arm64fs_debian adduser $USERNAME video
chroot /arm64fs_debian adduser $USERNAME render
chroot /arm64fs_debian adduser $USERNAME audio
chroot /arm64fs_debian adduser $USERNAME bluetooth
chroot /arm64fs_debian adduser $USERNAME plugdev
chroot /arm64fs_debian adduser $USERNAME input
chroot /arm64fs_debian adduser $USERNAME dialout

chroot /arm64fs_debian echo "$USERNAME:$PASSWORD" | chpasswd
chroot /arm64fs_debian echo "root:root" | chpasswd

# Remove apt packages which are no longer unnecessary and delete
# downloaded packages
chroot /arm64fs_debian apt -y autoremove --purge
chroot /arm64fs_debian apt clean

# Remove SSH keys and machine ID so they get generated on first boot
chroot /arm64fs_debian rm -f /etc/ssh/ssh_host_* \
      /etc/machine-id

# Disable getty on tty1, as we won't connect in console mode anyway
chroot /arm64fs_debian systemctl disable getty@.service

# FIXME: these are automatically installed on first boot, and block
# the system startup for over 1 minute! Find out why this happens and
# avoid this nasty hack
chroot /arm64fs_debian rm -f /lib/systemd/system/wpa_supplicant@.service \
      /lib/systemd/system/wpa_supplicant-wired@.service \
      /lib/systemd/system/wpa_supplicant-nl80211@.service
cd /


